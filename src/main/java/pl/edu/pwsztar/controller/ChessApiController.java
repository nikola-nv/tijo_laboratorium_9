package pl.edu.pwsztar.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.chess.Chess;

@Controller
@RequestMapping(value="/api")
public class ChessApiController {

    private Chess chess;

    @Autowired
    public ChessApiController(Chess chess){
        this.chess = chess;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(ChessApiController.class);



    @CrossOrigin
    @PostMapping(value = "/chess/is-correct-move")
    public ResponseEntity<Boolean> isCorrectMove(@RequestBody FigureMoveDto figureMoveDto) {
        LOGGER.info("*** move details : {}", figureMoveDto);
        boolean result = false;

        result = chess.figureMove(figureMoveDto);

        return ResponseEntity.ok(result);
    }
}
