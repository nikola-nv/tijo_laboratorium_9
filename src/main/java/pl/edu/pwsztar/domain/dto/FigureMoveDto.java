package pl.edu.pwsztar.domain.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.edu.pwsztar.domain.enums.FigureType;

import java.io.Serializable;

@NoArgsConstructor
@Getter
@ToString
public class FigureMoveDto implements Serializable {
    private String start;
    private String destination;
    private FigureType type;

    private FigureMoveDto (Builder builder){
        this.start = builder.start;
        this.destination = builder.destination;
        this.type = builder.type;
    }

    public FigureType getType() {
        return type;
    }

    public String getStart() {
        return start;
    }

    public String getDestination() {
        return destination;
    }


    public static final class Builder{
        private String start;
        private String destination;
        private FigureType type;

        public Builder start(String start){
            this.start = start;
            return this;
        }

        public Builder destination(String destination){
            this.destination = destination;
            return this;
        }

        public Builder type(FigureType type){
            this.type = type;
            return this;
        }

        public FigureMoveDto build(){
            return new FigureMoveDto(this);
        }
    }
}
