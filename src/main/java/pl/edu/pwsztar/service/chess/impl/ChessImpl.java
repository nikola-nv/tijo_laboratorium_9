package pl.edu.pwsztar.service.chess.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;import pl.edu.pwsztar.service.chess.Chess;
import pl.edu.pwsztar.service.figures.bishop.Bishop;

@Service
public class ChessImpl implements Chess {
    private Bishop bishop;

    @Autowired
    public ChessImpl(Bishop bishop){
        this.bishop = bishop;
    }

    @Override
    public boolean figureMove(FigureMoveDto figure){
        boolean result = false;

        switch(figure.getType()){
            case BISHOP:
                result = bishop.bishopMove(figure);
                break;
            default:
                System.out.println("There is no piss with this name " + figure.getType());
        }

        return result;
    }
}
