package pl.edu.pwsztar.service.chess;

import pl.edu.pwsztar.domain.dto.FigureMoveDto;

public interface Chess {
    boolean figureMove(FigureMoveDto figure);
}
