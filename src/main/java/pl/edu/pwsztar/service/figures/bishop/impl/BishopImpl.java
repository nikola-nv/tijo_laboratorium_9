package pl.edu.pwsztar.service.figures.bishop.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.service.figures.bishop.Bishop;



@Service
public class BishopImpl implements Bishop {

    @Autowired
    public BishopImpl() {

    }

    @Override
    public boolean bishopMove(FigureMoveDto move){
        String start = move.getStart();
        String destination = move.getDestination();

        String[] coordinates = start.split("_");

        int line = 97;
        int xIndex = Integer.parseInt(coordinates[1]);
        int yIndex = (int)coordinates[0].charAt(0) - line;


        if(start.equals(destination)){
            return false;
        }

        for(int x = xIndex , y = yIndex ;  x >= 0 && y >= 0 ; x-- , y--){
            String way = (char)(y+line) + "_" + x;
            if(way.equals(destination)){
                return true;
            }
        }

        for(int x = xIndex , y = yIndex ;  x >= 0 && y <= 8 ; x-- , y++){
            String way = (char)(y+line) + "_" + x;
            if(way.equals(destination)){
                return true;
            }
        }

        for(int x = xIndex , y = yIndex ;  x <= 8 && y >= 0 ; x++ , y--){
            String way = (char)(y+line) + "_" + x;
            if(way.equals(destination)){
                return true;
            }
        }

        for(int x = xIndex , y = yIndex ;  x <= 8 && y <= 8 ; x++ , y++){
            String way = (char)(y+line) + "_" + x;
            if(way.equals(destination)){
                return true;
            }
        }
        return false;
    }
}
