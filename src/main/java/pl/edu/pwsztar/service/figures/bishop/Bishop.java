package pl.edu.pwsztar.service.figures.bishop;

import pl.edu.pwsztar.domain.dto.FigureMoveDto;

public interface Bishop {
    boolean bishopMove(FigureMoveDto move);
}
